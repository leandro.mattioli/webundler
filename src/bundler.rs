#![warn(missing_docs)]

use std::path::{Path,PathBuf};
use std::collections::HashMap;
use std::fs::{File};
use std::future::Future;
use std::io;
use async_process;
use std::io::{Read, Write};
use std::io::Error as IOError;
use std::io::Result as IOResult;
use std::io::ErrorKind as IOErrorKind;
use std::process::Output;

use crate::assets_collection::AssetsCollection;
use crate::assets_watcher::AssetsWatcher;

/// JavaScript and CSS bundler
pub struct Bundler {
    collection: AssetsCollection,
    javascript_reverse_map: HashMap<String, Vec<String>>, // source => targets needing rebuild
}

impl Bundler {
    pub fn new(path: &str) -> Bundler {
        let collection = AssetsCollection::from_file(path)
            .expect(format!("JSON inválido: {}", path).as_str());
        let javascript_reverse_map = HashMap::default();
        let mut bundler =  Bundler {
            collection,
            javascript_reverse_map,
        };
        bundler.init();
        bundler
    }

    /// Part of the constructor... Making the compiler happy!
    pub fn init(&mut self) {
        // JavaScript rules
        for (target, sources) in &self.collection.javascript {
            for source in sources {
                let source_abspath = Bundler::abspath(source);
                let target_list = self.javascript_reverse_map
                    .entry(source_abspath).or_insert(Vec::default());
                target_list.push(target.to_string());
            }
        }
    }

    /// Get the absolute path from a string
    pub fn abspath(path: &str) -> String {
        let path = PathBuf::from(path).canonicalize().expect(
            &format!("Falha ao obter caminho absoluto para: {}", path)
        );
        path.to_string_lossy().to_string()
    }

    /// Concatenate files
    fn concatenate(sources: &Vec<String>) -> IOResult<Vec<u8>> {
        let mut contents = Vec::new();
        for source in sources {
            let mut file = File::open(source)?;
            file.read_to_end(&mut contents)?;
        }
        Ok(contents)
    }

    /// Enable filesystem watchers for auto-making and auto-copies
    pub fn config_watchers(&self, make_watcher: &mut AssetsWatcher) {
        for (target, _) in &self.javascript_reverse_map {
            make_watcher.watch(PathBuf::from(target));
        }
    }

    /// Make an asset (CSS or JavaScript)
    fn make_entry(&self, asset: &str) -> IOResult<()> {
        let extension = Path::new(asset).extension();
        if extension.is_none() {
            let err_message = format!("Missing extension in: {}", asset);
            return Err(IOError::new(IOErrorKind::Other, err_message));
        }
        let extension = extension.unwrap().to_string_lossy().to_string();
        let sources = &self.collection.javascript[asset];
        let content = Bundler::concatenate(sources)?;
        match extension.as_str() {
            /*"css" => {
                println!("  ==> Making asset {} ...", asset);
                let mut command = Command::new("sass")
                    .args(["--style", "compressed", "--no-source-map", "-q", "--stdin", asset])
                    .stdin(Stdio::piped())
                    .spawn()
                    .expect("Failed to run sass program!");
                let child_stdin = command.stdin.as_mut().expect("Failed to open stdin!");
                child_stdin.write_all(&content)?;
                command.wait()?;
                Ok(())
            },*/
            "js" => {
                println!("  ==> Making asset {} ...", asset);
                let mut command = std::process::Command::new("terser")
                    .args(["--compress", "--ecma", "6", "--output", asset])
                    .stdin(std::process::Stdio::piped())
                    .spawn()
                    .expect("Failed to run terser program!");
                let child_stdin = command.stdin.as_mut().expect("Failed to open stdin!");
                child_stdin.write_all(&content)?;
                command.wait()?;
                Ok(())
            },
            _ => {
                let err_msg = format!("Unknown format: {}", extension);
                Err(IOError::new(IOErrorKind::Other, err_msg))
            }
        }
    }

    pub fn notify(&self, id: &str, path: &str) {
        println!(" - Source-file changed: {}", path);
        if id == "javascript" {
            for target in &self.javascript_reverse_map[path] {
                let result = self.make_entry(&target);
                if result.is_err() {
                    eprintln!("ERROR: {:?}", result.unwrap_err());
                }
            }
        }
        println!("Press CTRL+C to quit");
    }

    /// Make and copy all targets (once)
    pub fn process_javascript(&self) {
        for target in self.collection.javascript.keys() {
            let result = self.make_entry(target);
            if result.is_err() {
                eprintln!("ERROR: {:?}", result.unwrap_err());
            }
        }
        
        //for target in self.collection.copy.keys() {
        //    self.make_entry(target)?;
        //}
    }

    pub fn launch_sass(&self, sass_watch: bool) -> impl Future<Output=io::Result<Output>> {
        println!("  ==> Processing SASS assets ...");
        let mut args = vec!["--style", "compressed", "--no-source-map", "-q"];
        if sass_watch {
            args.push("--watch");
        }
        let mut args: Vec<String> = args.iter().map(|&s| s.to_string()).collect();
        for (target, source) in self.collection.css.iter() {
            let pair = format!("{}:{}", source, target);
            args.push(pair);
        }
        let child = async_process::Command::new("sass").args(args).spawn()
                                                       .expect("Failed to run sass program!");
        child.output()
    }
}