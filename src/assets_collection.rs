use serde_json::{Value};
use std::fs;
use std::collections::HashMap;
use std::result::{Result};

/// A collection of assets needing to be built or copied to a target
pub struct AssetsCollection {
    /// Maps target to a list of sources
    pub javascript: HashMap<String, Vec<String>>, // target => sources
    /// Maps a target CSS to a source SCSS
    pub css: HashMap<String, String>       // target => source
}

impl AssetsCollection {
    ///Construct an assets collection from a JSON file
    pub fn from_file(filename: &str) -> Result<AssetsCollection, String> {
        let contents = fs::read_to_string(filename).map_err(
            |io_error| io_error.to_string()
        )?;
        let json : Value = serde_json::from_str(&contents).map_err(
            |serde_error| serde_error.to_string()
        )?;
        let mut collection = AssetsCollection { 
            javascript: HashMap::default(),
            css: HashMap::default()
        };
        let error_message = "Invalid assets file!".to_string();
        let root = json.as_object().ok_or(error_message.clone())?;
        //JavaScript rules
        let javascript_json = root.get("javascript").ok_or(error_message.clone())?;
        let javascript_map = javascript_json.as_object().ok_or(error_message.clone())?;
        for entry in javascript_map {
            let target = entry.0.to_string();
            let assets_json = entry.1.as_array().ok_or(error_message.clone())?;
            let mut assets : Vec<String> = Vec::new();
            for entry in assets_json {
                let asset = entry.as_str().ok_or(error_message.clone())?;
                assets.push(asset.to_string());
            }
            collection.javascript.insert(target, assets);
        }
        //CSS rules
        let css_json = root.get("css").ok_or(error_message.clone())?;
        let css_map = css_json.as_object().ok_or(error_message.clone())?;
        for entry in css_map {
            let target = entry.0.to_string();
            let source = entry.1.as_str().ok_or(error_message.clone())?;
            collection.css.insert(target, source.to_string());
        }
        Ok(collection)
    }
}