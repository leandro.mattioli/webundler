#![warn(missing_docs)]

use std::path::PathBuf;
use notify::{Watcher, RecursiveMode, watcher, DebouncedEvent, RecommendedWatcher};
use std::sync::mpsc::{channel, Receiver, TryRecvError};
use std::time::Duration;

use crate::bundler::Bundler;

/// FIXME (HARD): Remove dependency from "Bundler" class
/// without making the compiler complain about lifetimes and coerce closures to fns...

pub struct AssetsWatcher<'a> {
    id: String,
    rx: Receiver<DebouncedEvent>,
    filewatcher: RecommendedWatcher,
    bundler: &'a Bundler
}

/// Filesystem watcher
impl<'a> AssetsWatcher<'a> {
    pub fn new(id: String, bundler: &'a Bundler) -> Self {
        let (tx, rx) = channel();
        let delay = Duration::from_secs(1);
        let filewatcher = watcher(tx, delay).unwrap();
        Self { id, bundler, rx, filewatcher }
    }

    /// Watch a path for filesystem changes
    pub fn watch(&mut self, path: PathBuf) {
        self.filewatcher.watch(path, RecursiveMode::Recursive).unwrap();
    }

    /// Dispatch the handler
    /// # Arguments
    ///
    /// * `pathbuf` - Path to the source file that has been changed
    fn dispatch(&self, pathbuf: &PathBuf) {
        let source = pathbuf.to_string_lossy().to_string();
        self.bundler.notify(&self.id, &source);
    }

    /// Poll for filesystem changes and triggers events
    pub fn update(&self) {
        let result = self.rx.try_recv();
        match result {
            Err(TryRecvError::Empty) => return,
            Err(TryRecvError::Disconnected) => {
                println!("Thread communication error!")
            },
            Ok(DebouncedEvent::NoticeWrite(p)) => self.dispatch(&p),
            Ok(DebouncedEvent::NoticeRemove(p)) => self.dispatch(&p),
            Ok(DebouncedEvent::Remove(p)) => self.dispatch(&p),
            Ok(DebouncedEvent::Rename(p1, _)) => self.dispatch(&p1),
            Ok(DebouncedEvent::Write(p)) => self.dispatch(&p),
            Ok(DebouncedEvent::Create(p)) => self.dispatch(&p),
            Ok(DebouncedEvent::Chmod(p)) => self.dispatch(&p),
            Ok(DebouncedEvent::Rescan) => { },
            _ => {}
        }
    }
}