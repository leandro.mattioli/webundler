mod bundler;
mod assets_collection;
mod assets_watcher;

use assets_watcher::AssetsWatcher;
use std::env;
use std::path::PathBuf;
use std::time::Duration;
use clap::Parser;


#[derive(Parser)]
#[clap(version = "1.1", author = "Leandro R. Mattioli <leandro.mattioli@gmail.com>")]
struct Opts {
    /// JSON file describing assets generation rules (defaults to assets.json)
    #[clap(short, long, default_value = "assets.json")]
    assets_file: String,
    /// Run only once; don't watch for file changes (defaults to false)
    #[clap(long)]
    once: bool
}

fn main() {
    let opts : Opts = Opts::parse();
    // Chdir to assets.json folder
    let json_pathbuf = PathBuf::from(&opts.assets_file).canonicalize().unwrap();
    let json_filename = json_pathbuf.file_name().expect(
        &format!("Invalid JSON file: {}", &opts.assets_file)
    ).to_string_lossy().to_string();
    let json_folder = json_pathbuf.ancestors().skip(1).next().unwrap();
    let json_folder = json_folder.to_string_lossy().to_string();
    env::set_current_dir(&json_folder).expect("Cannot chdir to assets JSON folder.");
    // Read JSON and process all
    let bundler = bundler::Bundler::new(&json_filename);
    println!("Processing all entries...");
    bundler.process_javascript();
    let _future = bundler.launch_sass(!opts.once);
    //let mut finished = false;
    if !opts.once {
        /*if(!finished && future.poll() != Poll::Pending) {
            finished = true;
            println!("SASS has been closed or interrupted.")
        }*/
        let mut javascript_watcher = AssetsWatcher::new("javascript".to_string(), &bundler);
        bundler.config_watchers(&mut javascript_watcher);
        println!("Watching for changes...");
        println!("Press CONTROL+C to quit.");
        let pause_duration = Duration::from_millis(300);
        loop {
            javascript_watcher.update();
            std::thread::sleep(pause_duration);
        }
    }
}